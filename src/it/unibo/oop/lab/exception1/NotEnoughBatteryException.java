package it.unibo.oop.lab.exception1;

public class NotEnoughBatteryException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private double batteryLevel;
	
	public NotEnoughBatteryException(double batteryLevel) {
		super();
		this.batteryLevel = batteryLevel;
	} 
	
	public String getMessage() {
        return "Battery too low (" + this.batteryLevel + ")";
    }
}
