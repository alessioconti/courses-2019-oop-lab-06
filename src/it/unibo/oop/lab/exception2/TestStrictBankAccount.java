package it.unibo.oop.lab.exception2;

import org.junit.Test;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

/**
 * JUnit test to test
 * {@link StrictBankAccount}.
 * 
 */
public class TestStrictBankAccount {

    /**
     * Used to test Exceptions on {@link StrictBankAccount}.
     */
	private static final int INITIAL_AMOUNT = 10_000;
	
    @Test
    public void testBankOperations() {
        /*
         * 1) Creare due StrictBankAccountImpl assegnati a due AccountHolder a
         * scelta, con ammontare iniziale pari a 10000 e nMaxATMTransactions=10
         * 
         * 2) Effetture un numero di operazioni a piacere per verificare che le
         * eccezioni create vengano lanciate soltanto quando opportuno, cioè in
         * presenza di un id utente errato, oppure al superamento del numero di
         * operazioni ATM gratuite.
         */
    	
    	final AccountHolder usr1 = new AccountHolder("Mario", "Rossi", 1);
    	final AccountHolder usr2 = new AccountHolder("Luigi", "Bianchi", 2);
    	final StrictBankAccount account1 = new StrictBankAccount(usr1.getUserID(), INITIAL_AMOUNT, 10);
        final StrictBankAccount account2 = new StrictBankAccount(usr2.getUserID(), INITIAL_AMOUNT, 10);
    	
    	System.out.println("Deposito 1 in account1");
    	account1.deposit(001, 1);
    	System.out.println("Deposito 1 in account1 con id sbagliato");
    	try {
    		account1.deposit(002, 1);
    		fail();
    	} catch (WrongAccountHolderException e) {
    		assertNotNull(e);
    	}
    	
    	for (int i = 0; i < 10; i++) {
    		try {
    			account1.depositFromATM(usr1.getUserID(), 10);
    		} catch (TransactionsOverQuotaException | WrongAccountHolderException e) {
    			fail("Non sono ancora passato oltre al MaxATMTransactions");
    		}
    	}
    	
    	try {
    		account1.depositFromATM(usr1.getUserID(), 1);
    		fail("Dovrei aver raggiunto il max numero di ATMTransactions!");
		} catch (TransactionsOverQuotaException | WrongAccountHolderException e) {
			assertNotNull(e);
		}
    	
    	try {
    		account2.withdraw(usr2.getUserID(), 10_001);
    	} catch (WrongAccountHolderException e) {
    		fail();
    	} catch (NotEnoughtFoundException e) {
    		assertNotNull(e);
    	}
    	
    }
}
