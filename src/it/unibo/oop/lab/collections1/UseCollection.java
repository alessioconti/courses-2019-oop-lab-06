package it.unibo.oop.lab.collections1;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * Example class using {@link java.util.List} and {@link java.util.Map}.
 * 
 */
public final class UseCollection {
	
	private static final int ELEMS = 100_000;
    private static final int TO_MS = 1_000_000;

    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void main(final String... s) {
        /*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
        /*
         * 8) Compute the population of the world
         */
    	
    	
    	//1
    	ArrayList<Integer> arrayList = new ArrayList<>();
    	for (int i = 1000; i < 2000; i++) {
    		arrayList.add(i);
    	}
    	
    	//2
    	LinkedList<Integer> linkedList = new LinkedList<>();
    	linkedList.addAll(arrayList);
    	
    	//3
    	int tmp;
    	tmp = arrayList.get(0);
    	arrayList.set(0, arrayList.size()-1);
    	arrayList.set(arrayList.size()-1, tmp);
    	
    	//4
    	for (Integer elem : arrayList) {
    		System.out.println("" + elem);  	
    	}
    	
    	//5
    	long time = System.nanoTime();
        for (int i = 1; i <= ELEMS; i++) {
            arrayList.add(0, i);
        }
        time = System.nanoTime() - time;
        System.out.println("Addind " + ELEMS
                + " in the head of arrayList in " + time
                + "ns (" + time / TO_MS + "ms)");
        
        long time2 = System.nanoTime();
        for (int i = 1; i <= ELEMS; i++) {
            linkedList.add(0, i);
        }
        time2 = System.nanoTime() - time2;
        System.out.println("Addind " + ELEMS
                + " in the head of linkedList in " + time2
                + "ns (" + time2 / TO_MS + "ms)");
        
        //6
        long time3 = System.nanoTime();
        for (int i = 0; i <= 1000; i++) {
            arrayList.get(50_000);
        }
        time3 = System.nanoTime() - time3;
        System.out.println("Reading the element in index 50_000 for 1_000 times in " + time3
                + "ns (" + time3 / TO_MS + "ms)");
        
        long time4 = System.nanoTime();
        for (int i = 0; i <= 1000; i++) {
            linkedList.get(50_000);
        }
        time4 = System.nanoTime() - time4;
        System.out.println("Reading the element in index 50_000 for 1_000 times in " + time4
                + "ns (" + time4 / TO_MS + "ms)");
        
        //7
        HashMap<String, Long> continentsMap = new HashMap<>();
        continentsMap.put("Africa", (long) 1_110_635_000L);
        continentsMap.put("Americas", (long) 972_005_000L);
        continentsMap.put("Antarctica", (long) 0L);
        continentsMap.put("Asia", (long) 4_298_723_000L);
        continentsMap.put("Europe", (long) 742_452_000L);
        continentsMap.put("Oceania", (long) 38_304_000);
        
        //8
        long worldPopulation = 0;
        Collection<Long> continentsCollection = continentsMap.values();
        for (Long elem : continentsCollection) {
        	worldPopulation = worldPopulation + elem.longValue();
        }
        System.out.println("World Population: " + worldPopulation);
    }
}
