package it.unibo.oop.lab06.generics1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import it.unibo.oop.lab06.generics1.Node;

public class GraphImpl<N> implements Graph<N>{

	HashMap<N, List<N>> nodeMap = new HashMap<N, List<N>>();
	HashMap<N, Node<N>> nodeInfoMap = new HashMap<N, Node<N>>();
	
	@Override
	public void addNode(N node) {
		if (!nodeMap.containsKey(node)) {
			nodeMap.put(node, new ArrayList<N>());
			nodeInfoMap.put(node, new Node());
		}
	}

	@Override
	public void addEdge(N source, N target) {
		if (nodeMap.containsKey(source) && nodeMap.containsKey(target)) {
			nodeMap.get(source).add(target);
		}		
	}

	@Override
	public Set<N> nodeSet() {
		return nodeMap.keySet();
	}

	@Override
	public Set<N> linkedNodes(N node) {
		Set<N> setLinked = new HashSet<>();
		setLinked.addAll(nodeMap.get(node));
		return setLinked;
	}

	@Override
	public List<N> getPath(N source, N target) {
		// TODO Auto-generated method stub
		return null;
	}
	
	private void initializeBFS(N source) {
		for (Node<N> node: nodeInfoMap.values()) {
			node.setVisited(false);
			node.setDistanceFromSource(-1);
			node.setPredecessor(null);
		}
		nodeInfoMap.get(source).setVisited(true);
		nodeInfoMap.get(source).setDistanceFromSource(0);
		nodeInfoMap.get(source).setPredecessor(null);
	}
	
	private void BFS(N )

}
