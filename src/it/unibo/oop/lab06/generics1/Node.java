package it.unibo.oop.lab06.generics1;

public class Node<N> {
	private boolean visited;
	private int distanceFromSource;
	private N predecessorNode;
	
	public Node(){
		this.visited = false;
		this.distanceFromSource = Integer.MAX_VALUE;
		this.predecessorNode = null;
	}
	
	public boolean isVisited() {
		return this.visited;
	}
	public void setVisited(boolean visited) {
		this.visited = visited;
	}
	public int getDistanceFromSource() {
		return this.distanceFromSource;
	}
	public void setDistanceFromSource(int distanceFromSource) {
		this.distanceFromSource = distanceFromSource;
	}
	public N getPredecessor() {
		return this.predecessorNode;
	}
	public void setPredecessor(N predecessor) {
		this.predecessorNode = predecessor;
	}
	
}
